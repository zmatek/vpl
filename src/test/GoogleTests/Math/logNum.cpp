//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include <gtest/gtest.h>
#include <VPL/Math/Vector.h>
#include <VPL/Math/Algorithm/FuzzyCMeans.h>


namespace logNum
{

//! Test fixture
class LogNumTest : public testing::Test
{
public:
    vpl::math::CDLogNum n1;
    vpl::math::CDLogNum n2;
    vpl::math::CDLogNum n3;
    vpl::math::CDLogNum n;
    void SetUp() override
    {
        n = vpl::math::CDLogNum(5);
        n1 = vpl::math::CDLogNum(vpl::math::E);
        n2 = vpl::math::CDLogNum(0);
        n3 = vpl::math::CDLogNum(0, vpl::math::LogNum::LOG_VALUE);
    }
};

//! Testing initializing for some numbers
TEST_F(LogNumTest, Initialize)
{
    ASSERT_NEAR(1, n1.value,3);
    ASSERT_NEAR(2.71828,n1.get(),3);

    ASSERT_NEAR(-1e10, n2.value, 3);
    ASSERT_NEAR(0, n2.get(), 3);

    ASSERT_NEAR(0, n3.value, 3);
    ASSERT_NEAR(1, n3.get(), 3);

    ASSERT_NEAR(1.60944, n.value, 3);
    ASSERT_NEAR(5, n.get(), 3);

    n1 = 0;
    ASSERT_NEAR(-1e10, n2.value, 3);
    ASSERT_NEAR(0, n2.get(), 3);
}


TEST_F(LogNumTest, Multiply)
{
    n3 = n1 * n2;

    ASSERT_NEAR(-1e+10, n3.value, 3);
    ASSERT_NEAR(0, n3.get(), 3);

    n3 = n * 2.0;
    ASSERT_NEAR(2.30259, n3.value, 3);
    ASSERT_NEAR(10, n3.get(), 3);
}
TEST_F(LogNumTest, Divide)
{
    n3 = n1 / n2;

    ASSERT_NEAR(1e+10, n3.value, 3);
    ASSERT_TRUE(std::isinf(n3.get()));

    n3 = n / 2.0;
    ASSERT_NEAR(0.916291, n3.value, 3);
    ASSERT_NEAR(2.5, n3.get(), 3);
}

TEST_F(LogNumTest, Adding)
{
    n3 = n1 + n2;

    ASSERT_NEAR(1, n3.value, 3);
    ASSERT_NEAR(2.71828, n1.get(), 3);

    n3 = 2.0 + n;
    ASSERT_NEAR(1.94591, n3.value, 3);
    ASSERT_NEAR(7, n3.get(), 3);
}

TEST_F(LogNumTest, subtraction)
{
    n3 = n1 - n2;

    ASSERT_NEAR(1, n3.value, 3);
    ASSERT_NEAR(2.71828, n1.get(), 3);

    n3 = 10.0 - n;
    ASSERT_NEAR(1.60944, n3.value, 3);
    ASSERT_NEAR(5, n3.get(), 3);
}

TEST_F(LogNumTest, multiAdding)
{
    n1 = 0;
    for (int i = 0; i < 100; ++i)
    {
        n1 += 0.5;
    }

    ASSERT_NEAR(3.91202, n1.value,3);
    ASSERT_NEAR(50, n1.get(), 3);
}


}