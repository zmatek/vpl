//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include "gtest/gtest.h"

#include <VPL/Base/PartedData.h>
#include <VPL/Image/Image.h>
#include <VPL/Image/Volume.h>
#include <VPL/Image/VolumeFunctions.h>
#include <VPL/Module/Serialization.h>

#include <VPL/Test/Compare/compare3D.h>
#include <VPL/Test/Compare/compare2D.h>

namespace volume
{
typedef vpl::img::CVolume<vpl::img::tDensityPixel, vpl::base::CPartedData> tVolume;
typedef tVolume::tSmartPtr tVolumePtr;

class VolumeDTest : public testing::Test
{
public:
    tVolumePtr spVolume1;


    vpl::test::Compare3D<vpl::img::tDensityPixel, tVolume> compareVolume;
    vpl::test::Compare2D<vpl::img::tDensityPixel, vpl::img::CDImage> compareImage;


    void SetUp() override
    {
        spVolume1 = tVolumePtr(new tVolume(3, 3, 3, 1));

        compareVolume.enablePrintData(true);
        compareVolume.setErrorMessage("Volume is different at index");

        compareImage.enablePrintData(true);
        compareImage.setErrorMessage("Image is different at index");


    }

    static void generateSequence(tVolume& volume)
    {
	    for (vpl::tSize z = 0; z < volume.getZSize(); z++)
        {
            for (vpl::tSize y = 0; y < volume.getYSize(); y++)
            {
                for (vpl::tSize x = 0; x < volume.getXSize(); x++)
                {
                    volume(x, y, z) = z * volume.getYSize() * volume.getZSize() + y * volume.getXSize() + x;
                }
            }
        }
    }
};

TEST_F(VolumeDTest, SeDeserialization)
{
    tVolume volume1(50, 50, 10, 2);
    volume1.fill(1);
    vpl::mod::save(volume1, "dummy.vlm");
    tVolume volume2(256, 256, 256, 8);
    vpl::mod::load(volume2, "dummy.vlm");
    remove("dummy.vlm");
    compareVolume.values(1, volume2, volume2.getXSize(), volume2.getYSize(), volume2.getZSize());
}


TEST_F(VolumeDTest, ConstructorSize)
{
    tVolume volume(5, 10, 20, 1);
    ASSERT_EQ(5, volume.getXSize());
    ASSERT_EQ(10, volume.getYSize());
    ASSERT_EQ(20, volume.getZSize());
    ASSERT_EQ(1, volume.getMargin());
}


TEST_F(VolumeDTest, SmartPointer)
{
    ASSERT_EQ(3, spVolume1->getXSize());
    ASSERT_EQ(3, spVolume1->getYSize());
    ASSERT_EQ(3, spVolume1->getZSize());
    spVolume1->fillEntire(tVolume::tVoxel(123));

    compareVolume.values(123, *spVolume1, spVolume1->getXSize(), spVolume1->getYSize(), spVolume1->getZSize());
}


TEST_F(VolumeDTest, ConstructorRef)
{
    spVolume1->fillEntire(tVolume::tVoxel(123));
    tVolume volume2(*spVolume1, 1, 1, 1, 2, 2, 2, vpl::REFERENCE);

    vpl::img::tDensityPixel reqData[] =
    {
        0,1,2,3,
        4,5,6,7
    };
    generateSequence(volume2);

    compareVolume.values(reqData, volume2, volume2.getXSize(), volume2.getYSize(), volume2.getZSize());

    vpl::img::tDensityPixel reqData2[] =
    {
        123,123,123,
        123,123,123,
        123,123,123,
        123,123,123,
        123,0,1,
        123,2,3,
        123,123,123,
        123,4,5,
        123,6,7
    };
    compareVolume.values(reqData2, *spVolume1, spVolume1->getXSize(), spVolume1->getYSize(), spVolume1->getZSize());
}

TEST_F(VolumeDTest, Slice)
{
    generateSequence(*spVolume1);
    vpl::img::CDImage plane(3, 3, 2);
    plane.fillEntire(vpl::img::CDImage::tPixel(0));
    compareImage.valuesWithMargin(static_cast<vpl::img::tDensityPixel>(0), plane, plane.getXSize(), plane.getYSize(), plane.getMargin());

    vpl::img::tDensityPixel reqData1[] =
    {
        9,10,11,
        12,13,14,
        15,16,17,
    };

    spVolume1->getPlaneXY(1, plane);
    compareImage.values(reqData1, plane, plane.getXSize(), plane.getYSize());

    vpl::img::tDensityPixel reqData2[] =
    {
        6,7,8,
        15,16,17,
        24,25,26
    };

    spVolume1->getPlaneXZ(2, plane);
    compareImage.values(reqData2, plane, plane.getXSize(), plane.getYSize());
}
//!
TEST_F(VolumeDTest, Addition)
{
    spVolume1->fill(10);
    compareVolume.values(10, *spVolume1, spVolume1->getXSize(), spVolume1->getYSize(), spVolume1->getZSize());


    tVolume v(3, 3, 3, 1);
	v.fillEntire(1);
    *spVolume1 += v;
    compareVolume.values(11, *spVolume1, spVolume1->getXSize(), spVolume1->getYSize(), spVolume1->getZSize());
}

TEST_F(VolumeDTest, TestSequence)
{
	//generateSequence(*spVolume1);
}

TEST_F(VolumeDTest, MirrorMargin)
{
    { // shouldn't do anything (just don't crash)
        tVolume volume1(0, 0, 0, 1);
        volume1.mirrorMargin();
        tVolume volume2(0, 0, 0, 8);
        volume2.mirrorMargin();
    }
}

}
