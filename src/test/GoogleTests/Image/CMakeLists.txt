#################################################################################
# This file is part of
#
# VPL - Voxel Processing Library
# Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#################################################################################

set(TEST_NAME ImageTEST)

if( VPL_PNG_ENABLED )
    list( APPEND VPL_LIBS ${VPL_PNG} ${VPL_ZLIB} )
endif()


include_directories({VPL_SOURCE_DIR}/include/)
VPL_TEST_INCLUDE_DIR(${CMAKE_CURRENT_SOURCE_DIR})

ADD_HEADER_DIRECTORY( ${VPL_SOURCE_DIR}/include/gtest/Image)

if(VPL_GTESTS_PREBUILTS)
    include_directories(${GTEST_INCLUDE_DIRS})
endif()

# TinyXML
if( VPL_XML_ENABLED AND TINYXML_FOUND )
    INCLUDE_DIRECTORIES(${TINYXML_INCLUDE_DIR})
    LINK_DIRECTORIES(${TINYXML_LIBRARIES_DIR})
endif( VPL_XML_ENABLED AND TINYXML_FOUND)

ADD_SOURCE_DIRECTORY( ${CMAKE_CURRENT_SOURCE_DIR} )
add_executable(${TEST_NAME} ${VPL_SOURCES} ${VPL_HEADERS})

target_link_libraries( ${TEST_NAME} ${VPL_LIBS} ${TINYXML_LIBRARIES}   )
set_target_properties( ${TEST_NAME} PROPERTIES
                         DEBUG_POSTFIX d
                         LINK_FLAGS "${VPL_LINK_FLAGS}" )
						 

if( VPL_BUILD_WITH_GDCM )
	target_link_libraries( ${TEST_NAME}  ${VPL_ZLIB}  ${VPL_GDCM_LIBS})
endif( VPL_BUILD_WITH_GDCM )

if(VPL_GTESTS_PREBUILTS)
    target_link_libraries(${TEST_NAME}  ${GTEST_BOTH_LIBRARIES})
else()
    
    add_dependencies( ${TEST_NAME}  gtest )
    link_directories(${GTEST_DIRECTORY_LIBS})
    target_link_libraries( ${TEST_NAME} 
        debug ${GTEST_DEBUG_LIBRARIES}
        optimized ${GTEST_RELEASE_LIBRARIES}
 )
endif()

if(WIN32)

   INSTALL(FILES  ${VPL_3RDPARTY_DIR}/bin/libpng16.dll DESTINATION ${PROJECT_BINARY_DIR}/bin/Debug
   CONFIGURATIONS Debug)
   INSTALL(FILES  ${VPL_3RDPARTY_DIR}/bin/libpng16.dll DESTINATION ${PROJECT_BINARY_DIR}/bin/Release   CONFIGURATIONS Release)
   
   INSTALL(FILES  ${VPL_3RDPARTY_DIR}/bin/zlib.dll DESTINATION ${PROJECT_BINARY_DIR}/bin/Debug/
   CONFIGURATIONS Debug)
   INSTALL(FILES  ${VPL_3RDPARTY_DIR}/bin/zlib.dll DESTINATION ${PROJECT_BINARY_DIR}/bin/Release   CONFIGURATIONS Release)    
        
    INSTALL(FILES  ${VPL_3RDPARTY_DIR}/bin/libpng16.dll DESTINATION "test/gtest")
    INSTALL(FILES  ${VPL_3RDPARTY_DIR}/bin/zlib.dll DESTINATION "test/gtest")
endif()


INSTALL( TARGETS ${TEST_NAME}
        RUNTIME DESTINATION test/gtest )

add_test(AllTestsInMath ${TEST_NAME})


    

