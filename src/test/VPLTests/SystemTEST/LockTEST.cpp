//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2006 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.c
 * Date:    2006/01/18                          \n
 *
 * Description:
 * - Testing of the vpl::CLock class.
 */

#include <VPL/Base/Lock.h>
#include <VPL/System/Mutex.h>

#include <cstdlib>
#include <ctime>

// STL
#include <iostream>


//==============================================================================
/*
 * Global constants and variables.
 */

//! The number of testing loops.
const int COUNT     = 1000000;


//==============================================================================
/*
 * Global variables.
 */

//! Clock counter
clock_t clockCounter;


//==============================================================================
/*
 * Global functions.
 */

//! Waiting for a key
void keypress()
{
    while( std::cin.get() != '\n' );
}


//! Starts time measuring
void begin()
{
    clockCounter = clock();
}


//! Stops time measuring and prints results
void end()
{
    clockCounter = clock() - clockCounter;
    std::cout << "  Measured clock ticks: " << clockCounter << std::endl;
}


//==============================================================================
/*!
 * Class using mutex for mutual access to its members.
 */
class CMutexLock
{
public:
    void set(int iValue)
    {
        m_Lock.lock();
        m_iValue = iValue;
        m_Lock.unlock();
    }

protected:
    int m_iValue;
    vpl::sys::CMutex m_Lock;
};


//==============================================================================
/*!
 * Class using base::CLockableObject for mutual access to its members.
 */
class CObjectLock : public vpl::base::CLockableObject<CObjectLock>
{
public:
    typedef vpl::base::CLockableObject<CObjectLock>::CLock tLock;

public:
    void set(int iValue)
    {
        tLock Lock(*this);
        (void)Lock;

        m_iValue = iValue;
    }

protected:
    int m_iValue;
};


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    CMutexLock MLock;
    int i;
    std::cout << "Testing mutual access using mutex" << std::endl;
    begin();
    for( i = 0; i < COUNT; ++i )
    {
        MLock.set(i);
    }
    end();
    keypress();

    CObjectLock OLock;
    std::cout << "Testing mutual access using vpl::CLoackableObject" << std::endl;
    begin();
    for( i = 0; i < COUNT; ++i )
    {
        OLock.set(i);
    }
    end();
    keypress();

    return 0;
}

