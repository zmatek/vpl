//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/04                       
 *
 * Description:
 * - Testing of the vpl::CSharedMem class.
 */

#include <VPL/System/Sleep.h>
#include <VPL/System/NamedSemaphore.h>
#include <VPL/System/SharedMem.h>

#include <cstdio>
#include <cstring>
#include <cassert>

// STL
#include <iostream>
#include <string>


//==============================================================================
/*
 * Global constants and variables.
 */

//! Maximal length of a message
const int MAX_MSG_SIZE              = 1024;

//!    Named semaphore used for mutual exclusion
vpl::sys::CNamedSemaphore *pSemaphore   = NULL;

//! Shared memory
vpl::sys::CSharedMem *pSharedMem        = NULL;

//! Name of a semaphore which is used to access a shared memory
const std::string SEMAPHORE_NAME        = "1234";

//! Name of the shared memory
const std::string SHM_NAME              = "1234";


//==============================================================================
/*!
 * Shared structure
 */
struct SSharedData
{
    //! Size of the message stored in the shared memory
    int iMsg;

    //! Stored message
    char pcMsg[MAX_MSG_SIZE];
};


//==============================================================================
int main(int argc, char* argv[])
{
    // Check for a command line parameter
    bool bWriter = (argc == 2) && (strcmp(argv[1], "-writer") == 0);

    // Create/Open a named mutex
    bool bAlreadyExists = false;
    pSemaphore = new vpl::sys::CNamedSemaphore(1, &SEMAPHORE_NAME, &bAlreadyExists);
    if( bAlreadyExists )
    {
        std::cout << "Semaphore already exists." << std::endl;
    }

    // Create/Open the shared memory
    bAlreadyExists = false;
    pSharedMem = new vpl::sys::CSharedMem(SHM_NAME,
                                          sizeof(SSharedData),
                                          true,
                                          &bAlreadyExists
                                         );
    if( bAlreadyExists )
    {
        std::cout << "Shared memory already exists." << std::endl;
    }

    // Get pointer to the shared memory
    SSharedData *pData = (SSharedData *)pSharedMem->getData();
    assert(pData);

    // Init the shared structure
    if( bWriter )
    {
        pSemaphore->lock();
        pData->iMsg = 0;
    }

    // Writer
    if( bWriter )
    {
        bool bTerminate = false;
        while( !bTerminate )
        {
            // Init the message
            std::cout << "Writer: ";

            // Read the message
            fgets(pData->pcMsg, MAX_MSG_SIZE, stdin);

            // Terminate?
            bTerminate = (strcmp(pData->pcMsg, ".\n") == 0);

            // Message prepared
            pData->iMsg = (bTerminate) ? -1 : 1;

            // Send the message
            while( pData->iMsg != 0 )
            {
                // Release the semaphore
                pSemaphore->unlock();

                // Sleep for a while
                vpl::sys::sleep(100);

                // Wait for the semaphore
                pSemaphore->lock();
            }
        }
    }

    // Reader
    else
    {
        for( ;; )
        {
            // Wait for the semaphore
            pSemaphore->lock();

            // Check the message
            if( pData->iMsg == 1 )
            {
                // Print the message
                std::cout << "Slave: " << pData->pcMsg;

                // Update the message size
                pData->iMsg = 0;
            }
            else if( pData->iMsg == -1 )
            {
                // Update the message size
                pData->iMsg = 0;

                // Release the semaphore
                pSemaphore->unlock();

                // Terminate
                break;
            }

            // Release the semaphore
            pSemaphore->unlock();
        }
    }

    // Delete global variables
    delete pSharedMem;
    delete pSemaphore;

    return 0;
}

