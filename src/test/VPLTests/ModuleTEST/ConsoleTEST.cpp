//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/25                       
 *
 * Description:
 * - Testing of the vpl::mod::CConsole class.
 */

#include <VPL/System/Sleep.h>

#include "ConsoleTEST.h"

// STL
#include <iostream>
#include <string>


//==============================================================================
/*
 * Global module constants.
 */

//! Module description
const std::string MODULE_DESCRIPTION    = "Module writes copy of input data to the output channel";

//! Additional command line arguments
const std::string MODULE_ARGUMENTS      = "";


//==============================================================================
/*
 * Implementation of the class CConsoleTEST.
 */
CConsoleTEST::CConsoleTEST(const std::string& sDescription)
        : vpl::mod::CModule(sDescription)
{
    allowArguments(MODULE_ARGUMENTS);
}


CConsoleTEST::~CConsoleTEST()
{
}


bool CConsoleTEST::startup()
{
    // Note
    VPL_LOG_INFO("Module startup");

    // Test of existence of input and output channel
    if( getNumOfInputs() != 1 || getNumOfOutputs() != 1 )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Wrong number of input and output channels" << std::endl);
        return false;
    }

    // O.K.
    return true;
}


bool CConsoleTEST::main()
{
    // Note
    VPL_LOG_INFO("Module main function");

    // I/O channels
    vpl::mod::CChannel *pIChannel = getInput(0);
    vpl::mod::CChannel *pOChannel = getOutput(0);

    // Is any input?
    if( !pIChannel->isConnected() )
    {
        return false;
    }

    // Buffer
    char pcBuffer[512];

    // Wait for data
    if( pIChannel->wait(1000) )
    {
        // Read data from the input channel
        vpl::tSize iCount = pIChannel->read(pcBuffer, 512);
        if( iCount > 0 )
        {
            if( !pOChannel->write(pcBuffer, iCount) )
            {
                VPL_LOG_ERROR("Cannot write data to the output channel");
            }
        }
    }
    else
    {
        VPL_LOG_INFO("Wait timeout");
    }

    // Returning 'true' means to continue processing the input channel
    return true;
}


void CConsoleTEST::shutdown()
{
    // Note
    VPL_LOG_INFO("Module shutdown");
}


void CConsoleTEST::writeExtendedUsage(std::ostream&)
{
}


//==============================================================================
/*
 * Function main() which creates and executes console application.
 */
int main(int argc, char *argv[])
{
    // Creation of a module using smart pointer
    CConsoleTESTPtr spModule(new CConsoleTEST(MODULE_DESCRIPTION));

    // Initialize and execute the module
    if( spModule->init(argc, argv) )
    {
        spModule->run();
    }

    // Console application finished
    return 0;
}

